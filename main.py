import os
from os import path
import glob
import re
import shutil

def get_files_to_change(path_to_file):
    file_with_changes = open(path_to_file, 'r')
    read_file = file_with_changes.read()

    files_to_change = re.findall(r"\w+\.vhd:", read_file)

    files_to_change_2 = []
    for file_name in files_to_change:
        files_to_change_2.append(file_name[0:len(file_name)-1])

    return files_to_change_2


def parse_file(path_to_file, files_to_change):
    file_with_changes = open(path_to_file, 'r')
    lines = file_with_changes.readlines()

    changes_dict = {}
    vhd_ctr = 0

    lines = (line for line in lines if line)

    for line in lines:
        if line.strip() and line[0:2] != '//':
            if vhd_ctr < len(files_to_change) and re.match(files_to_change[vhd_ctr], line):
                # print("match", files_to_change[vhd_ctr], vhd_ctr)
                vhd_ctr = vhd_ctr + 1
                changes_dict[files_to_change[vhd_ctr - 1]] = []
                continue
            elif vhd_ctr > 0:
                changes_dict[files_to_change[vhd_ctr-1]].append(line)
            # else:
            #     print("ERROR: lines belongs to none file: ", lines)

    return changes_dict


def find_duplicates_in_list(input_list):
    sorted_list = sorted(input_list)
    duplicates = []

    for line in sorted_list:
        if line.strip() and line.split()[0] == '#' and sorted_list.count(line) > 1 and line not in duplicates:
             duplicates.append(line)

    if duplicates:
        return duplicates
    else:
        return 0


def find_duplicates(input_dict, log_file):
    for file_name in input_dict:
        duplicates = find_duplicates_in_list(input_dict[file_name])
        print(file_name)
        if duplicates:
            # print(duplicates)
            msg = "Found duplicated lines: " + str(duplicates) + "\n"
            log_file.write(msg)
        else:
            print("No duplicates")


def find_double_hash(input_dict):
    prev_line = ''
    doubled_hash = []

    for file_name in input_dict:
        for line in input_dict[file_name]:
            if line.strip() and line.split()[0] == '#' and prev_line.strip() and prev_line.split()[0] == '#':
                doubled_hash.append(prev_line)
                # print(prev_line)
            prev_line = line

    if doubled_hash:
        return doubled_hash
    else:
        return 0


def copy_files(dir_src, dir_desc, files_list, log_file):
    for file_name in files_list:
        text_files = glob.glob(dir_src + "/**/" + file_name, recursive=True)
        shutil.copyfile(text_files[0], dir_desc + file_name)
        print(text_files)

        if len(text_files) > 1:
            print("Found more files with name", file_name)
            msg = "Found more files with name" + str(file_name) + "\n"
            log_file.write(msg)
        elif len(text_files) == 0:
            print("Found no file with name", file_name)
            msg = "Found no file with name" + str(file_name) + "\n"
            log_file.write(msg)


def modify_files(in_dict, path_py_mod, log_file):

    changed_lines = []
    match = 0
    no_match = 0

    for file_name in in_dict:
        vhd_file = open(path_py_mod + file_name, 'r')
        lines_vhd = vhd_file.readlines()
        vhd_file.close()
        for input_line in in_dict[file_name]:
            if input_line.split()[0] == '#':
                vhd_line_ctr = 0
                match = 0
                changed_lines.append(input_line)
                # print("Found line to substitute")
                for line_vhd in lines_vhd:
                    vhd_line_ctr = vhd_line_ctr + 1
                    if line_vhd.strip() == input_line.strip('#').strip():
                        lines_vhd[vhd_line_ctr - 1] = '--' + input_line.strip('#').strip() + '\n'
                        match = 1
                        print("Matched line: ", vhd_line_ctr - 1)
                        print("Commented lined: ", input_line.strip('#').strip())
                        break
                if match == 0:
                    print("No match for line: ", input_line, "in file: ", file_name)
                    msg = "No match for line: " + str(input_line) + "in file: " + str(file_name) + "\n"
                    no_match = no_match + 1
                    log_file.write(msg)

            elif match == 1:
                vhd_file = open(path_py_mod + file_name, "w")
                lines_vhd.insert(vhd_line_ctr, input_line)
                vhd_file.writelines(lines_vhd)
                print("Added line     ", input_line)
                vhd_line_ctr = vhd_line_ctr + 1
                vhd_file.close()
    if no_match != 0:
        print(str(no_match), "lines not found. More info in errors_log.txt")
    else:
        print("All lines matched!")


if __name__ == '__main__':
    path_to_input_file = './changes_made_in_files_py.txt'
    path_to_rtl = './../../../alice-fit-fpga/'
    path_py_modified = './../src/py_modified/'
    path_to_log = './errors_log.txt'

    log_f = open(path_to_log, 'w')

    list_of_files = get_files_to_change(path_to_input_file)
    changes_dict = parse_file(path_to_input_file, list_of_files)

    find_duplicates(changes_dict, log_f)
    status = find_double_hash(changes_dict)

    if not os.path.exists(path_to_rtl):
        print("Directory", path_to_rtl, "not exist")
        log_f.write("Directory"+path_to_rtl+"not exist")
        exit(1)

    if not os.path.exists(path_py_modified):
        os.makedirs(path_py_modified)

    copy_files(path_to_rtl, path_py_modified, list_of_files, log_f)

    modify_files(changes_dict, path_py_modified, log_f)

    log_f.close()


