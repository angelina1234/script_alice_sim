# script_alice_sim

Put the files 'main.py' and 'changes_made_in_files_py.txt' in location '/alice-fit-fpga-sim/**new-sim-ver**/sim-script-py/', and just run by 'py main.py' or 'python3 main.py'.
Modified files will appear in '/alice-fit-fpga-sim/**new-sim-ver**/src/py_modified', logs file in '/alice-fit-fpga-sim/**new-sim-ver**/sim-script-py/'.

**new-sim-ver** - is your catalog name

Source catalog 'alice-fit-fpga' should be in the same location as (next to) 'alice-fit-fpga-sim'. 
If you want to change it, please modify in script main.py line: 148 "path_to_rtl = './../../../alice-fit-fpga/'"

Recommended Python version: 3.9.5